FROM node:16 as tsc-builder
WORKDIR /workspace
COPY . /workspace/
RUN npm i
RUN npx tsc

FROM node:16
WORKDIR /workspace
COPY package*.json /workspace/
RUN npm i --production
COPY --from=tsc-builder /workspace/dist /workspace/dist
EXPOSE $PORT
CMD npm start
