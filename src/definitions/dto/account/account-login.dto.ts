import {IsString, MinLength} from "class-validator";
import {Expose, Transform} from "class-transformer";
import {PasswordTransformHandler} from "../../../security.utils";

export class AccountLoginDTO {

    @MinLength(4)
    @IsString()
    @Expose()
    login: string;

    @MinLength(6)
    @IsString()
    @Expose()
    @Transform(PasswordTransformHandler)
    password: string;
}