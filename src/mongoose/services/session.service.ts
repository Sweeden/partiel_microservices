import {BaseService} from "./base.service";
import {Model, Mongoose} from "mongoose";
import {SessionSchema, ISessionDocument} from "../schemas";
import {SessionCreateDTO} from "../../definitions";

export class SessionService extends BaseService{
    protected model: Model<ISessionDocument>

    constructor(connection: Mongoose){
        super(connection);
        this.model = connection.model<ISessionDocument>('Session',SessionSchema);
    }

    public create(dto: SessionCreateDTO):Promise<ISessionDocument>{
        return this.model.create(dto)
    }

    public getByToken(token: string) : Promise<ISessionDocument | null>{
        return this.model.findOne({
            token
        }).populate('account').exec()
    }
}