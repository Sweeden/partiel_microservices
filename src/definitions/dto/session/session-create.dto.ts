import {IsDate, IsMongoId, IsOptional, IsString, MaxDate, MaxLength, MinLength} from "class-validator";
import {Expose, Transform, Type} from "class-transformer";
import {PasswordTransformHandler} from "../../../security.utils";

export class SessionCreateDTO {

    @MinLength(32)
    @MaxLength(32)
    @IsString()
    @Expose()
    token: string;


    @MinLength(1)
    @IsString()
    @Expose()
    platform: string;

    @IsMongoId()
    @Expose()
    account: string;
}