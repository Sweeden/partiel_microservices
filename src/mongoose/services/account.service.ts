import {BaseService} from "./base.service";
import {Model, Mongoose} from "mongoose";
import {AccountSchema, IAccountDocument} from "../schemas";
import {AccountCreateDTO, AccountLoginDTO} from "../../definitions";

export class AccountService extends BaseService{
    protected model: Model<IAccountDocument>

    constructor(connection: Mongoose){
        super(connection);
        this.model = connection.model<IAccountDocument>('Account',AccountSchema);
    }

    public create(dto: AccountCreateDTO):Promise<IAccountDocument>{
        return this.model.create(dto)
    }

    public getWithLogin(dto: AccountLoginDTO) : Promise<IAccountDocument | null>{
        return this.model.findOne(dto).exec();
    }
}