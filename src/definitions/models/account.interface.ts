export interface IAccount{
    _id: any;
    login: string;
    password: string;
    createdDate: Date;
    updatedDate: Date;
    firstName: string;
    lastName: string;
    birthDate?: Date;
}